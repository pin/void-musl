"vim configuration

colors monokai
set t_Co=256
set nocompatible
set backspace=indent,eol,start
set history=50
set ruler
syntax on
set number
set visualbell
set showmode
set showcmd
set cursorline
set showmatch
